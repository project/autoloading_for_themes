# Autoloading for themes
Out of the box, Drupal only honours files[] entries in module info files
and not themes.

Enable this module (and clear all caches) and all the files[] directives 
in your theme .info files will immediately begin working. 

Disabled themes are ignored.
